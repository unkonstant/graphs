package com.drobisch.graphs.layout.elk

import com.drobisch.graphs.Graph
import com.drobisch.graphs.defaults._
import com.drobisch.graphs.defaults.label._
import com.drobisch.graphs.layout.{DefaultGeometry, EdgePath, GraphLayout}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ELKJVMSpec extends AnyFlatSpec with Matchers with ScalaFutures {
  "Graph layout" should "layout simple graph" in {
    val graph =
      Graph.fromEdges[Unit, String](Set("A" --> "B", "B" --> "C", "D" --> "A"))
    ELKJVM.layout(graph).futureValue should be(
      GraphLayout(
        nodes = Map(
          "A" -> DefaultGeometry(100.0, 0.0, 80.0, 40.0),
          "B" -> DefaultGeometry(200.0, 0.0, 80.0, 40.0),
          "C" -> DefaultGeometry(300.0, 6.666666666666666, 80.0, 40.0),
          "D" -> DefaultGeometry(0.0, 0.0, 80.0, 40.0)
        ),
        edges = Map(
          "A-()-B" -> EdgePath(180.0, 20.0, 200.0, 20.0, List()),
          "B-()-C" -> EdgePath(280.0, 20.0, 300.0, 20.0, List()),
          "D-()-A" -> EdgePath(80.0, 20.0, 100.0, 20.0, List())
        ),
        Some(380.0),
        Some(46.666666666666664)
      )
    )
  }
}
