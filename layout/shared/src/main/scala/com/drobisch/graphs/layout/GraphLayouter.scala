package com.drobisch.graphs.layout

import com.drobisch.graphs.util.MathUtil
import com.drobisch.graphs.util.MathUtil.{LineSegment, Rectangle, Vector2}
import com.drobisch.graphs.{Edge, Graph, Labeled}

import scala.concurrent.Future

trait Geometry {
  def x: Double
  def y: Double
  def width: Double
  def height: Double
}

trait PointLike {
  def x: Double
  def y: Double
}

case class DefaultGeometry(x: Double, y: Double, width: Double, height: Double) extends Geometry

trait Cell {
  def geometry: Option[Geometry]
}

final case class EdgePath(
    sourceX: Double = 0.0,
    sourceY: Double = 0.0,
    targetX: Double = 0.0,
    targetY: Double = 0.0,
    points: List[PointSpec] = List.empty
)

object EdgePath {
  def intersected(from: Geometry, to: Geometry, points: List[PointSpec] = List.empty): EdgePath = {
    val fromCenterX = from.x + from.width / 2
    val fromCenterY = from.y + from.height / 2

    val toCenterX = to.x + to.width / 2
    val toCenterY = to.y + to.height / 2

    val sourceCenter = Vector2(fromCenterX, fromCenterY)
    val targetCenter = Vector2(toCenterX, toCenterY)
    val edgeSegment = LineSegment(sourceCenter, targetCenter)

    val sourceRect = Rectangle(
      Vector2(from.x, from.y),
      Vector2(from.x + from.width, from.y + from.height)
    )
    val targetRect = Rectangle(
      Vector2(to.x, to.y),
      Vector2(to.x + to.width, to.y + to.height)
    )

    val sourcePort =
      MathUtil.rectIntersect(edgeSegment, sourceRect).map(_ - sourceCenter)
    val targetPort =
      MathUtil.rectIntersect(edgeSegment, targetRect).map(_ - targetCenter)

    EdgePath(
      fromCenterX + sourcePort.map(_.x).getOrElse(0.0),
      fromCenterY + sourcePort.map(_.y).getOrElse(0.0),
      toCenterX + targetPort.map(_.x).getOrElse(0.0),
      toCenterY + targetPort.map(_.y).getOrElse(0.0),
      points
    )
  }

  def centered(from: Geometry, to: Geometry, points: List[PointSpec] = List.empty): EdgePath = {
    val fromCenterX = from.x + from.width / 2
    val fromCenterY = from.y + from.height / 2

    val toCenterX = to.x + to.width / 2
    val toCenterY = to.y + to.height / 2

    EdgePath(
      fromCenterX,
      fromCenterY,
      toCenterX,
      toCenterY,
      points
    )
  }

}

final case class PointSpec(x: Double, y: Double)

trait GraphLayoutLike {
  def nodeGeometry(id: String): Option[Geometry]
  def setNodeGeometry(id: String, geometry: Geometry): GraphLayoutLike

  def edgePath(id: String): Option[EdgePath]
  def setEdgePath(id: String, edgePath: EdgePath): GraphLayoutLike

  def updateNodePosition(
      id: String,
      fx: Double => Double,
      fy: Double => Double
  ): GraphLayoutLike

  def width: Option[Double]
  def height: Option[Double]

  /** convert this layout-like to a concrete value, mainly for serialization
    * @return
    *   a list of GraphLayout
    */
  def toGraphLayouts: List[GraphLayout]
}

final case class GraphLayout(
    nodes: Map[String, Geometry] = Map.empty,
    edges: Map[String, EdgePath] = Map.empty,
    width: Option[Double] = None,
    height: Option[Double] = None
) extends GraphLayoutLike {
  override def setNodeGeometry(id: String, geometry: Geometry): GraphLayout =
    copy(nodes = nodes + (id -> geometry))

  override def updateNodePosition(
      id: String,
      fx: Double => Double,
      fy: Double => Double
  ): GraphLayout =
    copy(nodes =
      nodes
        .get(id)
        .map(geo =>
          nodes + (id -> DefaultGeometry(
            x = fx(geo.x),
            y = fy(geo.y),
            geo.width,
            geo.height
          ))
        )
        .getOrElse(nodes)
    )

  override def setEdgePath(id: String, edgePath: EdgePath): GraphLayout =
    copy(edges = edges + (id -> edgePath))

  override def nodeGeometry(id: String): Option[Geometry] = nodes.get(id)

  override def edgePath(id: String): Option[EdgePath] = edges.get(id)

  override def toGraphLayouts: List[GraphLayout] = List(this)
}

final case class GraphLayouts(layouts: List[GraphLayout] = List.empty) extends GraphLayoutLike {
  private def updateFirst(update: GraphLayout => GraphLayout): GraphLayouts = {
    val updatedLayouts = layouts match {
      case Nil        => List(update(GraphLayout()))
      case head :: xs => update(head) :: xs
    }
    copy(layouts = updatedLayouts)
  }

  override def setNodeGeometry(
      id: String,
      geometry: Geometry
  ): GraphLayoutLike =
    updateFirst(_.setNodeGeometry(id, geometry))

  override def updateNodePosition(
      id: String,
      fx: Double => Double,
      fy: Double => Double
  ): GraphLayoutLike =
    updateFirst(_.updateNodePosition(id, fx, fy))

  override def setEdgePath(id: String, edgePath: EdgePath): GraphLayoutLike =
    updateFirst(_.setEdgePath(id, edgePath))

  override def nodeGeometry(id: String): Option[Geometry] =
    layouts.view.find(_.nodeGeometry(id).nonEmpty).flatMap(_.nodeGeometry(id))

  override def edgePath(id: String): Option[EdgePath] =
    layouts.view.find(_.edgePath(id).nonEmpty).flatMap(_.edgePath(id))

  override def width: Option[Double] =
    layouts.maxBy(_.width.getOrElse(0.0)).width

  override def height: Option[Double] =
    layouts.maxBy(_.height.getOrElse(0.0)).height

  override def toGraphLayouts: List[GraphLayout] = layouts
}

sealed trait LayoutDirection

object LayoutDirection {
  case object Up extends LayoutDirection
  case object Down extends LayoutDirection
  case object Left extends LayoutDirection
  case object Right extends LayoutDirection
}

sealed trait LayoutType

object LayoutType {
  case object Layered extends LayoutType
  case object Tree extends LayoutType
}

/** common configuration settings for layouts
  *
  * a layout implementation might not support all values mentioned here
  *
  * @param nodeWidth
  *   node width to consider during layout
  * @param nodeHeight
  *   node height to consider during layout
  * @param scale
  *   scale of the final result coordinates, mainly useful for force based layout
  * @param spacing
  *   general spacing of the layout
  * @param spacingNodeNode
  *   spacing between nodes
  * @param direction
  *   the direction in which the nodes will be layed out to
  * @param layoutType
  *   the general type of the layout
  * @param seed
  *   a seed for algorithms that use random values
  * @param edgePathIntersect
  *   if set the edges will start at the intersection of the node
  */
final case class GraphLayoutConfiguration(
    nodeWidth: Double = 80,
    nodeHeight: Double = 40,
    scale: Option[Double] = None,
    spacing: Option[Double] = None,
    spacingNodeNode: Option[Double] = None,
    direction: Option[LayoutDirection] = None,
    layoutType: Option[LayoutType] = None,
    seed: Option[Long] = None,
    edgePathIntersect: Boolean = false
)

trait GraphLayouter {
  def layout[E, N](
      g: Graph[E, N],
      layoutConfiguration: GraphLayoutConfiguration = GraphLayoutConfiguration()
  )(implicit edgeLabel: Labeled[Edge[E], String]): Future[GraphLayoutLike]
}

object GraphLayouter {
  val none: GraphLayouter = new GraphLayouter {
    def layout[E, N](
        g: Graph[E, N],
        layoutConfiguration: GraphLayoutConfiguration
    )(implicit edgeLabel: Labeled[Edge[E], String]): Future[GraphLayout] =
      Future.successful(GraphLayout())
  }
}
