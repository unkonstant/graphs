package com.drobisch.graphs.layout

import com.drobisch.graphs.Graph
import com.drobisch.graphs.defaults._
import com.drobisch.graphs.defaults.label._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ForceDirectedLayoutJVMSpec extends AnyFlatSpec with Matchers with ScalaFutures {
  "Force Directed layout" should "layout plants graph" in {
    val plants = Graph.fromEdges[Unit, String](
      Set(
        "Norway Spruce" --> "Sicilian Fir",
        "Sicilian Fir" --> "Sumatran Pine",
        "Sicilian Fir" --> "Japanese Larch",
        "Norway Spruce" --> "Japanese Larch",
        "Norway Spruce" --> "Giant Sequoia"
      )
    )

    val config = GraphLayoutConfiguration(seed = Some(42))
    ForceDirectedLayout.layout(plants, config).futureValue should be(
      GraphLayout(
        nodes = Map(
          "Sumatran Pine" -> DefaultGeometry(29.0, 178.0, 80.0, 40.0),
          "Giant Sequoia" -> DefaultGeometry(198.0, 85.0, 80.0, 40.0),
          "Sicilian Fir" -> DefaultGeometry(118.0, 0.0, 80.0, 40.0),
          "Norway Spruce" -> DefaultGeometry(170.0, 204.0, 80.0, 40.0),
          "Japanese Larch" -> DefaultGeometry(0.0, 34.0, 80.0, 40.0)
        ),
        edges = Map(
          "Norway Spruce-()-Giant Sequoia" -> EdgePath(210.0, 224.0, 238.0, 105.0, List()),
          "Norway Spruce-()-Sicilian Fir" -> EdgePath(210.0, 224.0, 158.0, 20.0, List()),
          "Sicilian Fir-()-Japanese Larch" -> EdgePath(158.0, 20.0, 40.0, 54.0, List()),
          "Norway Spruce-()-Japanese Larch" -> EdgePath(210.0, 224.0, 40.0, 54.0, List()),
          "Sicilian Fir-()-Sumatran Pine" -> EdgePath(158.0, 20.0, 69.0, 198.0, List())
        ),
        Some(278.0),
        Some(243.0)
      )
    )
  }
}
