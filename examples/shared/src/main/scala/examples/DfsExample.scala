package examples

import com.drobisch.graphs._
import com.drobisch.graphs.algorithm._
import com.drobisch.graphs.defaults._

trait DfsExample {
  val graph = Graph.fromEdges(
    Set(
      "1" --> "2",
      "1" --> "9",
      "2" --> "6",
      "2" --> "3",
      "3" --> "5",
      "3" --> "4",
      "6" --> "7",
      "6" --> "8"
    )
  )

  println(graph.dfs("1").steps.map(_.node.id))
  // List(1, 9, 2, 6, 8, 7, 3, 5, 4)
}
