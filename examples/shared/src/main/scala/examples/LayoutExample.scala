package examples

import com.drobisch.graphs.layout.{
  GraphLayoutConfiguration,
  GraphLayoutLike,
  GraphLayouter,
  LayoutType
}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait LayoutExample {
  def layouter: GraphLayouter
  def layoutConfig: GraphLayoutConfiguration =
    GraphLayoutConfiguration(nodeWidth = 40, layoutType = Some(LayoutType.Tree))

  // #layout_simple
  import com.drobisch.graphs._
  import com.drobisch.graphs.defaults.label._

  lazy val graph: Graph[Int, String] = DijkstraGraph.cities
  lazy val layout: Future[GraphLayoutLike] = layouter.layout(graph, layoutConfig)

  layout.onComplete(println)
  // #layout_simple
}
