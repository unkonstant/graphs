package examples

import com.drobisch.graphs._
import com.drobisch.graphs.algorithm._
import com.drobisch.graphs.defaults._

trait BfsExample {
  val graph = Graph.fromEdges(
    Set(
      "A" --> "D",
      "A" --> "C",
      "A" --> "B",
      "B" --> "E",
      "B" --> "F",
      "B" --> "G",
      "E" --> "H"
    )
  )

  println(graph.bfs("A").steps.map(_.node.id))
  // List(A, B, C, D, E, F, G, H)
}
