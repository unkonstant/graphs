package com.drobisch.graphs.view.util

import com.drobisch.graphs.layout.{EdgePath, GraphLayoutLike, PointSpec}
import com.drobisch.graphs.{Edge, Graph}

object DrawUtil {
  def getLinePoints[E, N](
      edge: Edge[E],
      graph: Graph[E, N],
      layout: GraphLayoutLike
  ): Option[Iterator[PointSpec]] = for {
    fromNode <- graph.findNode(edge.from)
    toNode <- graph.findNode(edge.to)
    from <- layout.nodeGeometry(fromNode.id)
    to <- layout.nodeGeometry(toNode.id)

    path = layout
      .edgePath(edge.id)
      .getOrElse(
        EdgePath(
          sourceX = from.x + from.width / 2,
          sourceY = from.y + from.height / 2,
          targetX = to.x + to.width / 2,
          targetY = to.y + to.height / 2
        )
      )
    points =
      Iterator(PointSpec(path.sourceX, path.sourceY)) ++
        path.points ++
        Iterator(PointSpec(path.targetX, path.targetY))
  } yield points
}
