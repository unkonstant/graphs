package com.drobisch.graphs.view

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import com.drobisch.graphs.Graph
import com.drobisch.graphs.defaults._
import com.drobisch.graphs.defaults.label._
import com.drobisch.graphs.layout.ForceDirectedLayout
import com.drobisch.graphs.style.StyleSheet
import com.drobisch.graphs.style.defaults._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.io.FileOutputStream

class SVGRendererJvmSpec extends AnyFlatSpec with Matchers {
  "Editor Renderer" should "render node" in {
    val graph: Graph[Unit, String] = Graph.fromEdges(Set("A" --> "B"))

    val renderer = SVGRendererJvm()
    val styleSheet = StyleSheet()

    val renderedSvg = for {
      layout <- IO.fromFuture(IO(ForceDirectedLayout.layout(graph)))
      xmlString <- renderer
        .translateAndScaleView(0, 0, 2)
        .renderGraph(ViewContext(graph, layout, styleSheet))
        .flatMap(_.toXmlString)
      _ <- IO {
        val fileOut = new FileOutputStream("target/test_simple.svg")
        fileOut.write(xmlString.getBytes("UTF-8"))
        fileOut.flush()
        fileOut.close()
      }
    } yield renderer.graphSVG

    renderedSvg.unsafeRunSync()
  }
}
