package com.drobisch.graphs.editor

import cats.effect.IO
import cats.effect.kernel.Ref
import cats.implicits._
import com.drobisch.graphs.Graph
import com.drobisch.graphs.editor.feature.{ModelUpdateFeature, RoutingFeature, UndoFeature}
import io.circe.Encoder

final case class EditorInstance(
    bus: EditorMessageBus,
    components: List[EditorComponent]
)

trait GraphEditor {
  def createEditorInstance[E, N](
      additionalComponents: EditorMessageBus => List[EditorComponent]
  )(configuration: EditorConfiguration, initialGraph: Option[Graph[E, N]] = None)(implicit
      edgeJson: Encoder[E],
      nodeJson: Encoder[N]
  ): IO[EditorInstance] = for {
    listeners <- Ref.of[IO, List[EditorComponent]](List.empty)
    log <- Ref.of[IO, List[EditorEvent]](List.empty)

    messageBus <- IO.pure(
      new EditorController(
        log,
        listeners,
        initial = EditorModel.fromConfig(configuration, initialGraph)
      )
    )

    components = List(
      new ModelUpdateFeature,
      new RoutingFeature,
      new UndoFeature
    ) ++ additionalComponents(messageBus)

    subscribed <- components
      .sortBy(_.order)
      .map(component => {
        messageBus.subscribe(component) *> IO(component)
      })
      .sequence
  } yield EditorInstance(messageBus, subscribed)
}
