package com.drobisch.graphs.editor.feature

import cats.effect.IO
import com.drobisch.graphs.editor._
import com.drobisch.graphs.layout.{EdgePath, GraphLayoutLike}
import com.drobisch.graphs.view._
import com.drobisch.graphs.{Edge, Node}

class RoutingFeature extends EditorComponent {

  override def order: Double = 0.2

  def edgePath(fromNode: Node[EditorGraphNode], toNode: Node[EditorGraphNode])(
      layout: GraphLayoutLike
  ): Option[EdgePath] =
    for {
      from <- layout.nodeGeometry(fromNode.id)
      to <- layout.nodeGeometry(toNode.id)
    } yield EdgePath.intersected(from, to)

  private def updateRouting(
      ctx: EditorContext,
      edge: Edge[EditorGraphEdge]
  ): EditorContext = {
    val fromNode = ctx.model.graph.findNode(edge.from)
    val toNode = ctx.model.graph.findNode(edge.to)

    val newLayout = for {
      from <- fromNode
      to <- toNode
      path <- edgePath(from, to)(ctx.model.layout)
    } yield ctx.model.layout.setEdgePath(edge.id, path)

    ctx
      .updateModel(_.updateLayout(current => newLayout.getOrElse(current)))
      .addNotification(
        this,
        ElementUpdated(ElementRef(edge.id, EdgeElementType), Internal)
      )
  }

  override def eval: Eval = ctx =>
    IO(ctx.transform {
      case ElementUpdated(ElementRef(id, EdgeElementType), Created, _) =>
        val newEdge = ctx.model.graph.findEdge(id)
        newEdge.map(updateRouting(ctx, _)).getOrElse(ctx)

      case ElementUpdated(ElementRef(id, NodeElementType), _, _) =>
        val edges = ctx.model.graph.incoming(id) ++ ctx.model.graph.outgoing(id)

        edges.foldLeft(ctx) { case (current, edge) =>
          updateRouting(current, edge)
        }
    })
}
