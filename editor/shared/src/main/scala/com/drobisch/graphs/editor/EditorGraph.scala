package com.drobisch.graphs.editor

import com.drobisch.graphs.layout.GraphLayoutLike
import com.drobisch.graphs.style.{StyleRef, StyleSheet}
import com.drobisch.graphs.{Edge, Graph, Identifiable, Labeled, Node}
import io.circe.{Decoder, Encoder, Json}
import io.circe.generic.semiauto._

trait EditorGraphElement {
  def id: String
  def data: Json
  def label: Option[String]
  def schemaRef: Option[String]
}

final case class EditorGraphNode(
    id: String,
    data: Json,
    stencil: Option[String],
    schemaRef: Option[String],
    label: Option[String] = None
) extends EditorGraphElement

final case class EditorGraphEdge(
    id: String,
    data: Json,
    connector: Option[String],
    schemaRef: Option[String],
    label: Option[String] = None
) extends EditorGraphElement

final case class EditorGraph(
    graph: Graph[EditorGraphEdge, EditorGraphNode],
    styleSheets: List[Either[String, StyleSheet]],
    layouts: List[Either[String, GraphLayoutLike]],
    schemas: List[Either[String, EditorModel.EditorSchema]]
)

object EditorGraph {
  import EditorGraphEdge.identifiableEditorEdge
  import EditorGraphNode.identifiableEditorNode

  def toEditorGraph[E, N](
      graph: Graph[E, N]
  )(implicit edgeJson: Encoder[E], nodeJson: Encoder[N]): Graph[EditorGraphEdge, EditorGraphNode] =
    Graph(
      edges = graph.edges.map(edge =>
        edge.copy(value = EditorGraphEdge(edge.id, edgeJson(edge.value), None, None, None))
      ),
      nodes = graph.nodes.map(node =>
        node.copy(value = EditorGraphNode(node.id, nodeJson(node.value), None, None, None))
      )
    )(identifiableEditorNode, identifiableEditorEdge)
}

object EditorGraphNode {
  implicit val identifiableEditorNode = new Identifiable[EditorGraphNode] {
    override def apply(value: EditorGraphNode): String = value.id
  }

  implicit val editorNodeStyleRef: StyleRef[Node[EditorGraphNode]] =
    new StyleRef[Node[EditorGraphNode]] {
      override def id(element: Node[EditorGraphNode]): Option[String] = Some(
        element.id
      )
      override def classList(element: Node[EditorGraphNode]): List[String] =
        element.value.stencil.toList
    }

  implicit val editorNodeLabel: Labeled[EditorGraphNode, String] =
    new Labeled[EditorGraphNode, String] {
      override def apply(node: EditorGraphNode): String =
        node.label.getOrElse("")
    }

  implicit lazy val encoder: Encoder[EditorGraphNode] = deriveEncoder[EditorGraphNode]
  implicit lazy val decoder: Decoder[EditorGraphNode] = deriveDecoder[EditorGraphNode]
}

object EditorGraphEdge {
  implicit lazy val identifiableEditorEdge = new Identifiable[EditorGraphEdge] {
    override def apply(value: EditorGraphEdge): String = value.id
  }

  implicit lazy val editorEdgeStyleRef: StyleRef[Edge[EditorGraphEdge]] =
    new StyleRef[Edge[EditorGraphEdge]] {
      override def id(element: Edge[EditorGraphEdge]): Option[String] = Some(
        element.id
      )
      override def classList(element: Edge[EditorGraphEdge]): List[String] =
        element.value.connector.toList
    }

  implicit val editorEdgeLabel: Labeled[EditorGraphEdge, String] =
    new Labeled[EditorGraphEdge, String] {
      override def apply(edge: EditorGraphEdge): String =
        edge.label.getOrElse("")
    }

  implicit lazy val encoder: Encoder[EditorGraphEdge] = deriveEncoder[EditorGraphEdge]
  implicit lazy val decoder: Decoder[EditorGraphEdge] = deriveDecoder[EditorGraphEdge]
}
