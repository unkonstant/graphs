package com.drobisch.graphs.editor

import cats.effect.IO
import com.drobisch.graphs.editor.view.SVGRendererJs
import com.drobisch.graphs.view.SVGRendererOptions
import org.scalajs.dom.raw.{Element, Event}

class EditorViewJs(container: Element)(messageBus: EditorMessageBus)
    extends EditorView[Element, Event](messageBus) {
  def createPage: IO[PageType] = for {
    newPage <- IO.pure(
      EditorPageJs(handleSelect, handleDrag, handleDoubleClick)(
        SVGRendererJs(SVGRendererOptions(showOrigin = true)),
        EditorDomEventLike
      )
    )
    currentPage <- pageRef.get
    _ <- currentPage match {
      case None =>
        IO(container.insertBefore(newPage.root, container.firstChild))
      case Some(existing) =>
        IO(container.replaceChild(newPage.root, existing.root))
    }
  } yield newPage
}
