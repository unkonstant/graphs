package com.drobisch.graphs.editor

import scala.scalajs.js.annotation.JSExportTopLevel

@JSExportTopLevel("graphs")
object EditorMainJs {
  def main(args: Array[String]): Unit = {
    println("graphs loaded...")
  }
}
