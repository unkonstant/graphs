package com.drobisch.graphs.editor

import cats.effect.IO
import org.scalajs.dom.raw.Element

class EditorPropertiesJs(container: Element)(val messageBus: EditorMessageBus)
    extends EditorProperties {

  override def initEditor(model: EditorModel): IO[Unit] = IO {
    container.appendChild(EditorPropertiesHtml.propertiesPanel)
    container.appendChild(EditorPropertiesHtml.propertiesToggle)
  }

  override def toggleEdit(enabled: Boolean): IO[Boolean] =
    IO(EditorPropertiesHtml.propertiesToggle.click()).attempt.map(_ => true)

  override def createPropertyControls(
      properties: List[PropertySpec],
      values: EditorPropertiesValues
  ): IO[List[PropertyControl]] = for {
    newForm <- EditorPropertiesHtml.createPropertyForm(properties)
    _ <- IO {
      if (EditorPropertiesHtml.propertiesBody.children.length == 0) {
        EditorPropertiesHtml.propertiesBody.appendChild(newForm.html)
      } else {
        EditorPropertiesHtml.propertiesBody.replaceChild(
          newForm.html,
          EditorPropertiesHtml.propertiesBody.firstChild
        )
      }
    }
  } yield newForm.controls
}
