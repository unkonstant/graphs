package com.drobisch.graphs.editor

import cats.effect.IO
import cats.effect.unsafe.IORuntime
import org.scalajs.dom.Event
import org.scalajs.dom.raw.Element

import scala.scalajs.js
import scala.scalajs.js.JSConverters._
import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}
import scala.scalajs.js.undefined

@JSExportTopLevel("GraphEditor")
object EditorJs extends GraphEditor {
  private implicit val ioRuntime: IORuntime = cats.effect.unsafe.implicits.global

  @JSExport
  def createEditor(
      containerElement: Element,
      optionsObj: js.UndefOr[js.Object],
      menuContainer: js.UndefOr[Element] = undefined,
      paletteContainer: js.UndefOr[Element] = undefined,
      propertiesContainer: js.UndefOr[Element] = undefined
  ): js.Promise[EditorInstanceJs] = (for {
    options <- optionsObj.toOption
      .map(obj => IO.fromEither(EditorConfiguration.decode(obj.toString)))
      .getOrElse(IO.pure(EditorConfiguration()))
    instance <- createEditorInstanceJs(
      containerElement,
      options,
      menuContainer.toOption,
      paletteContainer.toOption,
      propertiesContainer.toOption
    )
  } yield instance).unsafeToFuture().toJSPromise(ioRuntime.compute)

  def createEditorInstanceJs(
      containerElement: Element,
      config: EditorConfiguration,
      menuContainer: Option[Element],
      paletteContainer: Option[Element],
      propertiesContainer: Option[Element]
  ): IO[EditorInstanceJs] = (for {
    editor <- createEditorInstance[EditorGraphEdge, EditorGraphNode](bus =>
      List(
        Some(new EditorViewJs(containerElement)(bus)),
        propertiesContainer.map(new EditorPropertiesJs(_)(bus)),
        paletteContainer.map(new EditorPaletteJs(_)(bus)),
        menuContainer.map(new EditorMenuJs(_)(bus))
      ).flatten
    )(config)
    // we use right click for panning, prevent context menu
    _ <- IO(
      containerElement.addEventListener(
        "contextmenu",
        (event: Event) => {
          event.preventDefault()
        },
        false
      )
    )
  } yield new EditorInstanceJs(editor.bus))
    .redeemWith(
      error =>
        IO(println(s"error while creating editor $error")) *> IO.raiseError(
          error
        ),
      IO.pure
    )
}
