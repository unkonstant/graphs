package com.drobisch.graphs.algorithm

import com.drobisch.graphs.Graph
import com.drobisch.graphs.algorithm.Traversal.Step

class BreadthFirstTraversal[E, N](
    initialNodes: Iterable[String],
    graph: Graph[E, N]
) extends StepTraversal[E, N] {
  override def run: Iterable[TraversalEvent[Step[E, N]]] =
    Traversal.nodes(graph)(initialNodes.view.flatMap(graph.findNode))(
      TraversalState.queue
    )
}
